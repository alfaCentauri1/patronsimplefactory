package com.company;

public class Pizzeria {
    /**
     * El origen de los objetos puede ser una nueva instacia o una BD, un archivo, una API, ...
     * */
    public Pizza crearPizzaChica(String tipoPizza){
        return new Pizza(4, tipoPizza);
    }
    /**
     * El origen de los objetos en una nueva instanciación.
     * */
    public Pizza crearPizzaMediana(String tipoPizza){
        return new Pizza(8, tipoPizza);
    }

    public Pizza crearPizzaGrande(String tipoPizza){
        return new Pizza(12, tipoPizza);
    }
}
