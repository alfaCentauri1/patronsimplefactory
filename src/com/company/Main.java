package com.company;

public class Main {

    public static void main(String[] args) {
        Pizzeria pizzeria = new Pizzeria();
        pizzeria.crearPizzaChica("Napolitana");
        System.out.println("Ejemplo de Simple Factory.");
        pizzeria.crearPizzaGrande("Peperoni");
        System.out.println(pizzeria);
    }
}
